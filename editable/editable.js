let data 
let tableElem = document.createElement('table');
tableElem.className = 'table';
//
function createTable(elem){

let jsonURL =elem.getAttribute('data-source') 
fetch(jsonURL)
.then((response)=> response.json())
.then((lesDatasDuFichier) => {
data = lesDatasDuFichier 
tableHeader()
tableBody()
});   elem.appendChild(tableElem)
}
function tableHeader(){

let unElement = data[0]
let tr = document.createElement('tr')
for(let key in unElement){                  
let th = document.createElement('th')

th.onclick = function(){
if(typeof(unElement[key]) === 'string'){
 data.sort(function (a,b) {
 return a[key].localeCompare(b[key])   
                });
} else {
data.sort(function(a, b) { 
return a[key] - b[key];});
}
tableBody()
}
    th.innerText = key 
    tr.appendChild(th)
    }
    let thead= document.createElement('thead')
    thead.className = "thead"
    thead.appendChild(tr)
    tableElem.appendChild(thead)
}
function tableBody(){
    tableElem.querySelector('tbody')?.remove()// Selectionne "tbody" et ll existe deja
    let tbody= document.createElement('tbody')
    for(let obj of data){                           
    let tr = document.createElement('tr')
    for(let key in obj){
    let td = document.createElement('td')
    td.className = "table-data"
    td.contentEditable = true;

    td.innerText = obj[key] 
    td.addEventListener('input', e => {
    obj[key] = td.innerText;
    });

    tr.appendChild(td)
    }tbody.appendChild(tr)
    }

    tableElem.appendChild(tbody)
}
// Function to add a new row to the table
function addRow() {
    //create new empty array object
    let newData = {};
    let headers = tableElem.getElementsByTagName("th");
    //loop through the headers of the table
    for (let i = 0; i < headers.length; i++) {
        //prompt user for value of each header(th)
        newData[headers[i].innerText] = prompt("Enter value for " + headers[i].innerText + ":");
    }
    //add new datas to the newData Array
    data.push(newData);
    //call the tableBody function to update table
    tableBody();
}
// Function to create the add button
function createAddButton() {
    let button = document.createElement("button");
    button.innerText = "Add";
    button.addEventListener("click", addRow);
    let container = document.getElementById("container");
    container.appendChild(button);
}
    createAddButton();
   //create save button and add event listener 
   saveDiv = document.getElementById("save-button");
   saveDatas = document.createElement('button');
   saveDatas.innerText ="save";
   saveDiv.appendChild(saveDatas);
   //add eventlistener to button to send data to server
   saveDatas.addEventListener("click", function() {
   //convert the data to a json string 
    let dataJson = JSON.stringify(data)
    //use fetch API to send a POST request to the server with the data
    fetch('../test/up.php',{
    method: 'POST',
    body: dataJson,
})
})
function createSearchInput() {
    // Get the input element with id "search-input"
    let input = document.getElementById("search-input");
    
    // Add an event listener to the input element that listens for "keyup" events
    input.addEventListener("keyup", filterTable);
}
function filterTable() {
    // Get the value of the input field with id "search-input", convert it to lowercase
    let input = document.getElementById('search-input').value.toLowerCase();
    // Get the first tbody element within the table
    let tbody = tableElem.getElementsByTagName("tbody")[0];
    
    // Get all the tr elements within the tbody
    let tr = tbody.getElementsByTagName('tr')

    // Loop over all the tr elements
    for (let i = 0; i < tr.length; i++) {
        // Get all the td elements within the current tr
        let dataCells = tr[i].getElementsByTagName("td");

        // Assume there is no match found in the current row
        let match = false;
        // Loop over all the td elements within the current tr
        for (let j = 0; j < dataCells.length; j++) {
            // Check if the input string is found within the text of the current td
            if (dataCells[j].innerText.toLowerCase().indexOf(input) > -1) {
                // If a match is found, set match to true
                match = true;
                break;
            }
        }
        // If a match was found in the current row, display the row
        if (match) {
            tr[i].style.display = "";
        } 
        // If no match was found, hide the row
        else {
            tr[i].style.display = "none";
        }
    }
}



